import numpy as np
from scipy.stats import zipf
from main import generate_users, start_game

log_test = open("logs/test.txt", 'w')

repeat = 10000
zipf_factor = 1.05
file_number = 100
cache_size = 60
user_number =10


# ordered = np.zeros(number)
# ordered_norm = np.zeros(number)
# frac_k_largest = np.zeros(number)
# frac_mean_without_k_largest = np.zeros(number)

utilities = np.zeros(user_number)


def log_mean_utility(trial):
    for utility in utilities:
        log_test.write("%s\t" % (utility/(trial+1)))
    log_test.write("\n")
    log_test.flush()


for trial in range(repeat):
    user_list = generate_users(user_number, file_number,cache_size, zipf_factor)

    current_cache_vec = np.zeros(file_number)
    current_cache_vec, step_index, total_utility = start_game(current_cache_vec, cache_size, file_number, user_number,
                                                              user_list)
    for user in user_list:
        utilities[user.id] += user.get_utility(current_cache_vec)
    log_mean_utility(trial)


    # rand_vec = np.random.random(number)
    # print np.mean(rand_vec)
    # sorted_vec = -np.sort(-rand_vec)
    # normalized_vec = sorted_vec / sum(sorted_vec)
    #
    # ordered += sorted_vec
    # ordered_norm += normalized_vec
    #
    # for index in range(number):
    #     frac_k_largest[index] += sum(normalized_vec[range(index+1)])
    #     frac_mean_without_k_largest[index] += np.mean(normalized_vec[range(index+1, len(normalized_vec))])

# print ordered/repeat
# print ordered_norm/repeat
# print frac_k_largest/repeat
#
# log_test.write("Absolute values\n")
# for value in ordered:
#     log_test.write("%s\t" % (value/repeat))
# log_test.write("\n")
# log_test.write("Percentage values\n") #\t%s\n" % (ordered_norm/repeat))
# for value in ordered_norm:
#     log_test.write("%s\t" % (value/repeat))
# log_test.write("\n")
# log_test.write("Largest k percentage\n") # \t%s\n" % (frac_k_largest/repeat))
# for value in frac_k_largest:
#     log_test.write("%s\t" % (value/repeat))
# log_test.write("\n")
# log_test.write("Mean percentage without k largest\n") # \t%s\n" % (frac_mean_without_k_largest/repeat))
# for value in frac_mean_without_k_largest:
#     log_test.write("%s\t" % (value/repeat))
# log_test.write("\n")


log_test.close()