import sys
from VCG_Mech import VCG_Mech
from Isolated_Allocator import Isolated_Allocator
import numpy as np
import datetime
import math

# Parameters:
## 0: Int, Cache size
## 1: list of int, file sizes;
## 2: list of list of float, each list represents the cache preferences of a user;

def OpuS(R, file_sizes, peferences):

    file_number = len(file_sizes)
    #parse user preferences
    user_number = len(peferences)
    PMatrix = np.zeros([user_number, file_number])
    for user_index in range(0, user_number):
        PMatrix[user_index] = np.array(peferences[user_index])


    result = False
    try:
        result = VCG_Mech(PMatrix, file_sizes, R)
    except:
        print "OpuS fails to share"
        return False
    if result == False:
        print "OpuS fails to share"
        return False

    print "OpuS shares the cache"
    (PMatrix_unified, cached_ratio, access_factor, final_utility) = result
    #cached_ratio_string = ','.join(str("{:10.4f}".format(e)) for e in cached_ratio)
    #access_factor_string = ','.join(str("{:10.4f}".format(e)) for e in access_factor)
    #print(cached_ratio_string)
    #print(access_factor_string)
    return final_utility


if __name__ == "__main__":
    OpuS(sys.argv[1:])
    #OpuS(["1,1,2", "1,2,5", "2,3,4", "3,4,10", "4,3,20"])
    #OpuS(["9394592,9394592,9394592,9394592,9394592,9394592,9394592,9394592,9394592","0,3,0,7,3,1,0,3,0", "1,2,4,2,2,0,1,3,2", "2,2,3,3,0,0,4,1,1"])
