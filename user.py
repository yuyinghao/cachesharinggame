import numpy as np
import math
from scipy.stats import zipf

class User():
    def __init__(self, user_id, cache_budget, file_number, zipf_factor):
        self.id = user_id
        self.budget = cache_budget
        self.preference = np.zeros(file_number)
        self.sorted_preference_indices = np.zeros(file_number)
        self.choice_vec = np.zeros(file_number) # owned by this users
        self.zipf_factor = zipf_factor

    def get_utility(self, cache_vec):
        return np.inner(cache_vec, self.preference )

    def get_potential(self):
        return np.inner(self.choice_vec, self.preference)

    def make_choice(self, current_cache_vec): # should handle the case where files are partially cached
                                                # should specify whether an action has been made

        # clear its own choices first
        current_cache_vec -= self.choice_vec

        incomplete_file_indices = [i for i, v in enumerate(current_cache_vec) if v <= 1] #
        remaining_budget = self.budget
        this_choice_vec = np.zeros(len(current_cache_vec))
        for index in self.sorted_preference_indices:
            if index in incomplete_file_indices and remaining_budget > 0:
                this_choice_vec[index] = min(remaining_budget, 1 - current_cache_vec[index])
                remaining_budget -= this_choice_vec[index]
            if remaining_budget <= 0:
                break

        if np.array_equal(this_choice_vec, self.choice_vec):
            return (False, this_choice_vec + current_cache_vec)
        else:
            self.choice_vec = this_choice_vec
            return (True, this_choice_vec + current_cache_vec)

    def generate_zip_preference(self):
        file_number = len(self.preference)
        for i in range(0, file_number):
            #self.preference[i] = zipf.pmf(i+1, self.zipf_factor)
            #self.preference[i] = np.random.random(1)
            self.preference[i] = np.random.exponential(1)
        # normalize
        self.preference /= sum(self.preference)

        self.preference = np.random.permutation(self.preference) # each user loves different files


        self.sorted_preference_indices = np.argsort(-self.preference) # from large to small
                             # user may cache files with p = 0 instead of redistributing the leftover budget

    def get_isolation_ulility(self):
        full_file_number =int(np.floor(self.budget))
        partial = self.budget - full_file_number

        sorted_preference = -np.sort(-self.preference) # from large to small
        isolation_utility = np.sum(sorted_preference[range(0, full_file_number)]) + partial * sorted_preference[
            full_file_number]

        return isolation_utility

