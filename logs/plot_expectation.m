hold off
user_number = 2:1:7;

plot(user_number, optimal(1:length(user_number)),'k-', user_number, optimal_simu(1:length(user_number)), 'ro', user_number, game(1:length(user_number)),'g-', user_number, game_simu(1:length(user_number)),'b+', 'linewidth',2)
legend('Optimal-Expectation','Optimal-Simulation', 'Game-Expectation', 'Game-Simulation','location','best')
set(gca,'fontsize',18)
grid on
set(gca, 'XTick', user_number)
xlabel('User #')
ylabel('Sum of cache hit ratio')


saveas(gcf,'figures/expectation.fig');
saveas(gcf,'figures/expectation.eps','epsc');
