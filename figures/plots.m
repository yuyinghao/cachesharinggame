hold off
[y,x]=ecdf(unif_1000_100);
plot(x,y,'linewidth',3)

set(gca, 'fontsize',18)
grid on
ylabel('CDF')
xlabel('Price of Anarchy')
saveas(gcf,'PoA.eps','epsc')

hold on
[y,x]=ecdf(zipf_1000_100);
plot(x,y,'linewidth',3)
legend('Uniform distributed preference', 'Zipf distributed preference','location','best')
saveas(gcf,'PoA_distr.eps','epsc')


%user number
hold off
x_label_vec = [30,50,80,100,150,200];
y_vec = [mean(unif_1000_30),mean(unif_1000_50),mean(unif_1000_80),mean(unif_1000_100),mean(unif_1000_150),mean(unif_1000_200)];
bar(5:5:30, y_vec, 0.4)
axis([0 35 0.7 1])
set(gca, 'xticklabels',x_label_vec)
set(gca, 'fontsize',18)
grid on
ylabel('Average PoA')
xlabel('User #')
saveas(gcf,'user_number.eps','epsc')

%file number
hold off
x_label_vec = [300,500,800,1000,1200];
y_vec = [mean(unif_300_100),mean(unif_500_100),mean(unif_800_100),mean(unif_1000_100),mean(unif_1200_100)];
bar(5:5:25, y_vec, 0.4)
axis([0 30 0.7 1])
set(gca, 'xticklabels',x_label_vec)
set(gca, 'fontsize',18)
grid on
ylabel('Average PoA')
xlabel('File #')
saveas(gcf,'file_number.eps','epsc')



%verify sum of k largest i.i.d, uniform distribtued 
hold off
sum_k_largest_analytic = arrayfun(@(x)(x/1000 * (2001-x)/1001), 1:1000);
plot( 1:1000,sum_k_largest_analytic,'r-', 1:1000, sum_k_largest_simu, 'k--','linewidth',3)
set(gca, 'fontsize',18)
grid on
%ylabel({'Aggregated preference';'of the k most prefered files'})
ylabel({'Lowerbound of the Expected PoA'})
xlabel('k')
legend('Analytic', 'Simulation','location','southeast')
saveas(gcf,'sum_k_largest.eps','epsc')