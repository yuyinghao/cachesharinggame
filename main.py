
import numpy as np
from cvxpy import *
import math
import datetime
from user import User
from OpuS import OpuS
from Isolated_Allocator import Isolated_Allocator
from FairRide_allocator import FairRide_allocator
def main():

    # parameters
    file_number = 32
    #cache_size = 60
    cache_size_per_user = 4
    user_number = 20
    zipf_factor = 1.1
    repeat = 10000

    # log files
    log_initial_step = open("logs/init_step.txt", 'w')
    log_initial_price = open("logs/init_price.txt", 'w') # price compared to the global optimum
    log_initial_util = open("logs/init_util.txt", 'w')
    log_initial_time = open("logs/init_time.txt", 'w') # time to reach equilibrium from empty cache
    #log_initial_price_fair = open("logs/init_price_fair.txt", 'w')
    log_init_improve_isolation = open("logs/init_improve_isolation.txt", 'w') # improvement over isolation


    #log_convergence_step = open("logs/converge_step.txt", 'w')
    #log_convergence_price = open("logs/converge_price.txt", 'w')
    #log_convergence_price_fair = open("logs/converge_price_fair.txt", 'w')


    log_OpuS_price = open("logs/OpuS_price.txt", 'w') # price compared to the global optimum
    log_OpuS_util = open("logs/OpuS_util.txt", 'w')
    log_OpuS_time = open("logs/OpuS_time.txt", 'w')  # time to allocate the cache from empty cache

    log_FairRide_price = open("logs/FairRide_price.txt", 'w') # price compared to the global optimum
    log_FairRide_util = open("logs/FairRide_util.txt", 'w')
    log_FairRide_time = open("logs/FairRide_time.txt", 'w')  # time to allocate the cache from empty cache

    log_isolation_util = open("logs/isolation_util.txt", 'w')
    log_optimal_util = open("logs/optimal_util.txt", 'w')

 # try absolute cache access frequency
    optimal_util = 0
    for user_number in range(2, 9, 1): #(50,151,5)
        for trial in range(repeat):
            cache_size = cache_size_per_user * user_number
            user_list = generate_users(user_number, file_number, cache_size, zipf_factor)

            #start_game
            start_time = datetime.datetime.now()
            current_cache_vec = np.zeros(file_number)
            current_cache_vec, step_index, total_utility = start_game(current_cache_vec,cache_size, file_number,user_number, user_list)
            end_time = datetime.datetime.now()
            log_initial_step.write("%s\t" % step_index)
            log_initial_util.write("%s\t" % total_utility)
            log_initial_time.write("%s\t" % (end_time - start_time).total_seconds())

            preferences = list()
            for user in user_list:
                preferences.append(user.preference)
            #OpuS
            # start_time = datetime.datetime.now()
            # opus_utility = OpuS(cache_size, np.ones(file_number),preferences)
            # if isinstance(opus_utility, bool): # OpuS failed to share the cache
            #     opus_utility = get_isolation(user_list)
            #     print "OpuS", opus_utility
            # else:
            #     print "OpuS", sum(opus_utility)
            #     opus_utility = sum(opus_utility)
            # log_OpuS_util.write("%s\t" % opus_utility)
            # end_time = datetime.datetime.now()
            #
            # log_OpuS_time.write("%s\t" % (end_time - start_time).total_seconds())

            # #FairRide
            # start_time = datetime.datetime.now()
            # fairRide_utility = FairRide_allocator(preferences,np.ones(file_number),cache_size)
            #
            # print "FairRide", sum(fairRide_utility)
            # end_time = datetime.datetime.now()
            # log_FairRide_util.write("%s\t" % sum(fairRide_utility))
            # log_FairRide_time.write("%s\t" % (end_time - start_time).total_seconds())



            optimal_utility = get_optimial(user_list, cache_size, file_number)
            print "Optimal", optimal_utility
            optimal_util += optimal_utility
            log_optimal_util.write("%s\t" % optimal_utility)
            print optimal_util /repeat
            # optimal_utility_fair = get_optimal_fair(user_list, cache_size, file_number)
            # print optimal_utility_fair

            # isolation_utility = get_isolation(user_list)
            # log_isolation_util.write("%s\t" % isolation_utility)
            # print 'Isolation', isolation_utility





            log_initial_price.write("%s\t" % (total_utility / optimal_utility))
            #log_OpuS_price.write("%s\t" % (opus_utility/ optimal_utility))
            # log_FairRide_price.write("%s\t" % (sum(fairRide_utility) / optimal_utility))
            # log_init_improve_isolation.write("%s\t" % (total_utility /isolation_utility))
            #log_initial_price_fair.write("%s\n" % (total_utility / optimal_utility_fair))

        log_initial_step.write("\n")
        log_initial_step.flush()
        log_initial_util.write("\n")
        log_initial_util.flush()
        log_initial_time.write("\n")
        log_OpuS_util.write("\n")
        log_initial_time.flush()
        log_OpuS_time.write("\n")
        log_OpuS_time.flush()
        log_FairRide_util.write("\n")
        log_FairRide_util.flush()
        log_FairRide_time.write("\n")
        log_FairRide_time.flush()
        log_optimal_util.write("\n")
        log_optimal_util.flush()
        log_isolation_util.write("\n")
        log_isolation_util.flush()
        log_initial_price.write("\n")
        log_initial_price.flush()
        log_OpuS_price.write("\n")
        log_OpuS_price.flush()
        log_FairRide_price.write("\n")
        log_FairRide_price.flush()
        log_init_improve_isolation.write("\n")
        log_init_improve_isolation.flush()





        # # now randomly choose a user to change his preference and check how it converges
            # user_index = np.random.randint(user_number)
            # user_list[user_index].generate_zip_preference()
            # current_cache_vec, step_index, total_utility, optimal_utility, optimal_utility_fair, isolation_utility = start_game(current_cache_vec,cache_size, file_number,
            #                                                                                 user_number, user_list)
            # log_convergence_step.write("%s\n" % step_index)
            # log_convergence_price.write("%s\n" % (total_utility / optimal_utility))
            # log_convergence_price_fair.write("%s\n" % (total_utility / optimal_utility_fair))
            # log_convergence_improve_isolation.write("%s\n" % (total_utility / isolation_utility))

def start_game(current_cache_vec, cache_size, file_number,user_number, user_list):

    step_index = 0
    while True:
        this_step_index = step_index
        #order = np.random.permutation(user_number) # ask in a round robin way
        order = range(0, user_number) # ask in sequence
        for user_id in order:
            (result, current_cache_vec) = user_list[user_id].make_choice(current_cache_vec)
            if result == True:  # had an action
                step_index += 1
        if (this_step_index == step_index):  # no one wants to change, NE achieved
            break

    print step_index
    total_utility = get_utility(user_list, current_cache_vec)
    #print current_cache_vec
    print 'Game', total_utility

    return current_cache_vec, step_index, total_utility

def generate_users(user_number,file_number, cache_size, zipf_factor):
    user_list = list()
    for user_id in range(user_number):
        user = User(user_id, cache_size* 1.0 /user_number, file_number, zipf_factor)
        user.generate_zip_preference()
        user_list.append(user)
    return user_list

def get_utility(user_list, current_cache_vec):
    total_utility = 0
    for user in user_list:
        total_utility += user.get_utility(current_cache_vec)
    return total_utility


def get_optimial(user_list, cache_size, file_number):
    aggregate_preference = np.zeros(file_number)
    for user in user_list:
        aggregate_preference += user.preference

    full_file_number = int(np.floor(cache_size))
    partial = cache_size - full_file_number

    sorted_preference = -np.sort(-aggregate_preference) # from large to small
    optimal_utility = np.sum(sorted_preference[range(0,full_file_number)]) + partial * sorted_preference[full_file_number]

    return optimal_utility



def get_optimal_fair(user_list, cache_size, file_number): # optimal result considering isolation guarantee
    aggregate_preference = np.zeros(file_number)
    for user in user_list:
        aggregate_preference += user.preference

    # linear programming
    x = Variable(file_number)  # allocation vector
    objective = Maximize(sum_entries(aggregate_preference * x))
    constraints = [0 <= x, x <= 1, sum_entries(x) <= cache_size]
    for user in user_list:
        constraints.append(sum_entries(user.preference * x) >= user.get_isolation_ulility())
    prob = Problem(objective, constraints)
    try:
        prob.solve()
        ##print "Optimal value", result

        # print "Preference Matrix"
        # print PMatrix

        # print "Optimal var"
        # print x.value
        if prob.status == 'optimal':
            # print np.squeeze(np.asarray(x.value))
            return np.inner(np.squeeze(np.asarray(x.value)), aggregate_preference)
        else:
            return False
    except:
        return False

def get_isolation(user_list):
    isolation_utility_total = 0
    for user in user_list:
        isolation_utility_total += user.get_isolation_ulility()
    return isolation_utility_total

if __name__ == '__main__':
    main()